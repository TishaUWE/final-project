#include <iostream>
#include <conio.h>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <Windows.h>
#include <cstdio>
using namespace std;

class Node
{
public:
    int id, stockId, marketPrice, qtyPurchased, qtyCompany, qtySold; 
    string name, address, dob, stockName;

    Node *next_add;
};

class Linked_List
{
public:
    Node *head = NULL;
    void Insert()
    {
        fstream file;
        file.open("Items.txt"), ios::app;

        int i; // r = i
        string n;
        string a;  // c=a
        string d;  // e=d
        int si;    // sid = si
        int mp;    // marketprice
        string sn; // stockname
        int q;     // qtypurchased
        int qc;
        int qs;    // sold

        cout << "\n\n\t\t\t\t Nepal Stock Analzer";
        cout << "Stocklist \n";
        cout << "\n";
        cout << "StockID" << "\t\t\t" << "StockName" << "\t\t" << "MarketPrice(Current)" << "\t\t" << " QtyPurchased" << "\n";
        cout << "========================================================================================================|\n";
        cout << "1" << "\t\t\t" << "Apple" << "\t\t\t" << "$95" << "\t\t\t\t" << "1000" << "\n";
        cout << "2" << "\t\t\t" << "Tesla" << "\t\t\t" << "5" << "\t\t\t\t" << "3000" << "\n";
        cout << "3" << "\t\t\t" << "VW" << "\t\t\t" << "$71" << "\t\t\t\t" << "2000" << "\n";
        cout << "\n";
        cout << "\n";
    
        cout << "Client details";
        cout <<"\n\nName : ";
        cin >> n;
        cout << "ID : ";
        cin >> i;
        cout << "Address : ";
        cin >> a;
        cout << "Date of birth : ";
        cin >> d;
        cout << "Id of stock : ";
        cin >> si;
        cout << "Name of stock : ";
        cin >> sn;
        cout << "Current market price of the stock : ";
        cin >> mp;
        cout << "Total number of share purchased: ";
        cin>>q;
        cout << "Total number of share sold: ";
        cin >> qs;

        Node *new_node = new Node;
        new_node->id = i;
        new_node-> name= n;
        new_node-> address= a;
        new_node-> dob= d;
        new_node-> stockId= si;
        new_node-> stockName= sn;
        new_node-> marketPrice = mp;
        new_node-> qtyPurchased = q;
        new_node-> qtyCompany = qc;
        new_node-> qtySold= qs;
        new_node->next_add = NULL;

        file << new_node-> id << "\n";
        file << new_node->name << "\n";
        file << new_node-> address << "\n";
        file << new_node-> dob << "\n";
        file << new_node-> stockId<< "\n";
        file << new_node-> stockName<< "\n";
        file << new_node-> marketPrice<< "\n";
        file << new_node-> qtyPurchased<< "\n";
        file << new_node-> qtyCompany<< "\n";
        file << new_node-> qtySold<< "\n";
        file.close();

        if (head == NULL)
        {
            head = new_node;
        }
        else
        {
            Node *ptr = head;

            while (ptr->next_add != NULL)
            {
                ptr = ptr->next_add;
            }

            ptr->next_add = new_node;
        }
        cout << "\n Data registered";
    }

    void Search()
    {
        if (head == NULL)
        {
            cout << "\n\n  No data.";
        }

        else
        {
            int r, found = 0;
            cout << " \n\n Portfolio ";
            cout << "\n\n Enter Client Id for search: ";
            cin >> r;
            Node *ptr = head;
            while (ptr != NULL)
            {
                if (r == ptr->id)
                {  
                    cout << "\n\n\t\t\t\t Nepal Stock Analzer\n";
                    cout << "Stocklist \n";
                    cout << "\n";
                    cout << "StockID" << "\t\t\t" << "StockName" << "\t\t" << "MarketPrice(Current)" << "\t\t" << " QtyPurchased" << "\n";
                    cout << "========================================================================================================|\n";
                    cout << "1" << "\t\t\t" << "Apple" << "\t\t\t" << "$95" << "\t\t\t\t" << "1000" << "\n";
                    cout << "2" << "\t\t\t" << "Tesla" << "\t\t\t" << "5" << "\t\t\t\t" << "3000" << "\n";
                    cout << "3" << "\t\t\t" << "VW" << "\t\t\t" << "$71" << "\t\t\t\t" << "2000" << "\n";
                    cout << "\n";
                    cout << "\n";

                    cout << "Client " << ptr->name;
                    cout << "\n";
                    cout << "S.No " << "\t\t\t" << "Field" << "\t\t\t" << "Description \n";
                    cout << "=================================================================|\n";
                    cout << "1" << "\t\t\t" << "Name : " <<  "\t\t\t\t\t" << ptr->name << "\n";
                    cout << "2" << "\t\t\t" << "Id : " << "\t\t\t\t\t" << ptr->id  << "\n" ; 
                    cout << "3" << "\t\t\t" << "Address : " << "\t\t\t\t" << ptr->address << "\n";
                    cout << "4" << "\t\t\t" << "Date of birth : " << "\t\t\t" << ptr->dob << "\n";
                    cout << "\n";
                    cout << "\n";
                    cout << "PurchaseTable";
                    cout << "\n";
                    cout << "S.No " << "\t\t\t" << "Field" << "\t\t\t" << "Description \n";
                    cout << "=================================================================|\n";
                    cout << "1" << "\t\t\t" << "StockId : " << "\t\t\t\t" << ptr->stockId<< "\n";
                    cout << "2" << "\t\t\t" << "STockName : " << "\t\t\t\t" << ptr->stockName<< "\n";
                    cout << "3" << "\t\t\t" << "MarketPrice(Current) : " << "\t\t\t" << ptr->marketPrice << "\n";
                    cout << "4" << "\t\t\t" << "QtyPurchased(Total): " << "\t\t\t" << ptr->qtyCompany<< "\n";
                    cout << "\n";
                    cout << "\n";
                    cout << "StockSold";
                    cout << "\n";
                    cout << "S.No " << "\t\t\t" << "Field" << "\t\t\t" << "Description \n";
                    cout << "=================================================================|\n";
                    cout << "1" << "\t\t\t" << "StockId : " << "\t\t\t\t" << ptr->stockId << "\n";
                    cout << "2" << "\t\t\t" << "StockName : " << "\t\t\t\t" << ptr->stockName<< "\n"; 
                    cout << "3" << "\t\t\t" << "MarketPrice : " << "\t\t\t\t" << ptr->marketPrice << "\n";
                    cout << "4" << "\t\t\t" << "Qty purchased : " << "\t\t\t" << ptr->qtySold<< "\n";
                    
                    found++;
                }

                ptr = ptr->next_add;
            }
            if (found == 0)
            {
                cout << "\n\n Id :" << r << "dont validate";
            }
        }
    }

    void Count()
    {
        if (head == NULL)
        {
            cout << "Data empty!!";
        }
        else
        {
            int c = 0;
            Node *ptr = head;
            while (ptr != NULL)
            {
                c++;
                ptr = ptr->next_add;
            }

            cout << "\n\n Number of clients :" << c;
        }
    }

    void Update()
    {
        if (head == NULL)
        {
            cout << "Data empty!!";
        }
        else
        {
            int r, found = 0;
            cout << "\n\n Enter ID For Updation";
            cin >> r;

            Node *ptr = head;
            while (ptr != NULL)
            {
                if (r == ptr->id)
                {
                    cout << "\n\n Name:";
                    cin>>ptr->name;
                    cout << "\n\n ID:";
                    cin>>ptr->id;
                    cout << "\n\n Address:";
                    cin>>ptr->address;
                    cout << "\n\n Date of birth";
                    cin>>ptr->dob;
                    cout << "\n\nRecord Updated Successfully:";
                    found++;
                }

                ptr = ptr->next_add;
            }

            if (found == 0)
            {
                cout << " Id " << r << "Can't Found";
            }
        }
    }

    void Del()
    {
        if (head == NULL)
        {
            cout << "Linked list is Empty";
        }
        else
        {
            int r, found = 0;
            cout << "\n\n Enter ID for DELETION";
            cin >> r;

            if (r == head->id)
            {
                Node *ptr = head;
                head = head->next_add;
                cout << "\n\n Record Deleted Successfully";
                found++;
                delete ptr;
            }
            else
            {
                Node *pre = head;
                Node *ptr = head->next_add;
                while (ptr != NULL)
                {
                    if (r = ptr->id)
                    {
                        pre->next_add = ptr->next_add;
                        cout << "\n\n Record Deleted Successfully";
                        found++;
                        delete ptr;
                        break;
                    }

                    pre = ptr;
                    ptr = ptr->next_add;
                }
            }

            if (found == 0)
            {
                cout << "\n\nDelete ID;" << r << "Can't Found";
            }
        }
    }

    void Show()
    {
        if (head == NULL)
        {
            cout << "\n\n Data empty!!";
        }

        else
        {
            int r, found = 0;
            cout << "\n\n Enter client Id: ";
            cin >> r;
            Node *ptr = head;
            while (ptr != NULL)
            {
                if (r == ptr->id)
                {  
                   
                   cout << "\n\n\t\t\t\t Nepal Stock Analzer";
                    cout << "Stocklist \n";
                    cout << "\n";
                    cout << "StockID" << "\t\t\t" << "StockName" << "\t\t" << "MarketPrice(Current)" << "\t\t" << " QtyPurchased" << "\n";
                    cout << "========================================================================================================|\n";
                    cout << "1" << "\t\t\t" << "Apple" << "\t\t\t" << "$95" << "\t\t\t\t" << "1000" << "\n";
                    cout << "2" << "\t\t\t" << "Tesla" << "\t\t\t" << "5" << "\t\t\t\t" << "3000" << "\n";
                    cout << "3" << "\t\t\t" << "VW" << "\t\t\t" << "$71" << "\t\t\t\t" << "2000" << "\n";
                    cout << "\n";
                    cout << "\n";

                    cout << "\n\n\n\t\t\tclient";

                    cout << "\n\t\t\tS.NO    Field                                     Description                        " << endl;

                    cout << "\n\n\t\t\t 1.    Name\t\t\t\t\t\t"<< ptr->name;
                    cout << "\n\t\t\t 2.     Id\t\t\t\t\t\t" << ptr->id;
                    cout << "\n\t\t\t 3.     Address\t\t\t\t\t\t" << ptr->address;
                    cout << "\n\t\t\t 4.     Date of birth\t\t\t\t\t" << ptr->dob;

                    cout<< "\n\n\n\n\t\t\t PurchaseTable";
                    cout << "\n\n\t\t\t 1.     StockId\t\t\t\t\t\t" << ptr->stockId;
                    cout << "\n\t\t\t 2.     StockName\t\t\t\t\t" << ptr->stockName;
                    cout << "\n\t\t\t 3.     MarketPrice(Current)\t\t\t\t" << ptr->marketPrice;
                    cout << "\n\t\t\t 4.     Total number of share purchased by a company\t" << ptr->qtyCompany;

                    cout<< "\n\n\n\n\t\t\tStockSold";
                    cout << "\n\n\t\t\t 1.     StockId\t\t\t\t\t\t" << ptr->stockId;
                    cout << "\n\t\t\t 2.     StockName\t\t\t\t\t" << ptr->stockName;
                    cout << "\n\t\t\t 3.     MarketPrice\t\t\t\t\t" << ptr->marketPrice;
                    cout << "\n\t\t\t 4.     Total number of share sold by a company\t\t" << ptr->qtySold;                    
                    ptr = ptr->next_add;
                }
            }
        }
    }

    
   
    
};


int main()
{
    Linked_List list;
    Linked_List obj;
p:
    system("cls");
    int choice;
    cout<<"Welcome";
    cout << "\n\n\t\t\t\t Nepal Stock Analyzer";
    cout << "\n\n\n\t\t\t------------------------"<<endl;

    cout << "\n\t\t\t 1. New client registration";
    cout << "\n\t\t\t 2. Account portfolio ";
    cout << "\n\t\t\t 3. Count";
    cout << "\n\t\t\t 4. Update";
    cout << "\n\t\t\t 5. Delete";
    cout << "\n\t\t\t 6. Exit";
    cout << "\n\t\t\t  ~ Enter choice......";
    
    cin >>  choice;
    
    switch (choice)
    {
    case 1:
        system("cls");
        obj.Insert();
        break;
    case 2:
        system("cls");
        obj.Search();
        break;
    case 3:
        system("cls");
        obj.Count();
        break;
    case 4:
        system("cls");
        obj.Update();
        break;
    case 5:
        system("cls");
        obj.Del();
        break;
  
    case 6:

        exit(0);
  
    default:
        cout << "\n\n\n Invalid Choice";
    }

    getch();
    goto p;

   
    return 0;
}


//Name: Tisha Manandhar
//BSc(Hons)CyberSecurity and Digital Forensics
//UWEID:22085550

